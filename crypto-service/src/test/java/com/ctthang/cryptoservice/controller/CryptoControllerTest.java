package com.ctthang.cryptoservice.controller;

import com.ctthang.cryptoservice.dto.CryptoDTO;
import com.ctthang.cryptoservice.dto.ResponeEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
class CryptoControllerTest {
    
    @LocalServerPort
    int randomServerPort;
    
    private WebClient webClient;
    private String url;
    
    
    @BeforeEach
    void setUp() {
        url = "http://localhost:" + 8081;
        webClient = WebClient.create(url);
    }
    
    @Test
    void getListCrypto() {
        ResponeEntity res = webClient.get()
                .uri("/api/cryptos")
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        List<CryptoDTO> cryptoDTOList = (List<CryptoDTO>) res.getData();
        assertEquals(200, res.getStatus());
        assertEquals(5,cryptoDTOList.size());
    }
    
    
    @Test
    void getCryptoById() {
        long id = 2;
        ResponeEntity res = webClient.get()
                .uri("/api/cryptos/{id}",id)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        Integer cryptoId = (Integer) ((LinkedHashMap) res.getData()).get("id");
        assertEquals(200, res.getStatus());
        assertEquals(2,cryptoId);
    }
    
    @Test
    void getCryptoFilter() {
        String symbol = "BTC",address = null,name="Bitcoin";
        ResponeEntity res = webClient.get()
                .uri("/api/cryptos/search?symbol={symbol}&name={name}",symbol,name,address)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        List<LinkedHashMap> cryptoDTOList = (List<LinkedHashMap>) res.getData();
        assertEquals(200,res.getStatus());
        cryptoDTOList.forEach(cryptoDTO -> {
            assertEquals(symbol,cryptoDTO.get("symbol"));
        });
        
    }
    
    @Test
    void getCurrentPriceOfSymbol() {
        String symbol = "BTC";
        ResponeEntity res = webClient.get()
                .uri("/api/cryptos/current-price?symbol={symbol}",symbol)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
        BigDecimal price = BigDecimal.valueOf((Double) ((LinkedHashMap) res.getData()).get("current_price"));
        assertNotNull(price);
    }
    
    @Test
    void getPriceOfSymbolAtTime() {
        String symbol = "BTC";
        Long time = Instant.now().getEpochSecond();
        ResponeEntity res = webClient.get()
                .uri("/api/cryptos/price-time?symbol={symbol}&time_at={time}",symbol,time)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
        BigDecimal price = BigDecimal.valueOf((Double) (res.getData()));
        assertNotNull(price);
    }
    
    @Test
    void createNewCrypto() {
        CryptoDTO cryptoDTO = new CryptoDTO();
        cryptoDTO.setSymbol("NEW SYMBOL");
        cryptoDTO.setUrl("new url");
        cryptoDTO.setName("new name");
        cryptoDTO.setCoinGeckoId("new coint gecko");
        cryptoDTO.setWhitelistCollateral(Boolean.TRUE);
        cryptoDTO.setWhitelistSupply(Boolean.TRUE);
        cryptoDTO.setAddress("349drsd");
        cryptoDTO.setIsDeleted(Boolean.FALSE);
        ResponeEntity res = webClient.post()
                .uri("/api/cryptos")
                .body(Mono.just(cryptoDTO),CryptoDTO.class)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
        String symbol = String.valueOf (((LinkedHashMap) res.getData()).get("symbol"));
        assertEquals(cryptoDTO.getSymbol(),symbol);
    }
    
    @Test
    void updateCrypto() {
        CryptoDTO cryptoDTO = new CryptoDTO();
        cryptoDTO.setId(18698L);
        cryptoDTO.setSymbol("SYMBOL UPDATED");
        cryptoDTO.setUrl("new url");
        cryptoDTO.setName("new name");
        cryptoDTO.setCoinGeckoId("new coint gecko");
        cryptoDTO.setWhitelistCollateral(Boolean.TRUE);
        cryptoDTO.setWhitelistSupply(Boolean.TRUE);
        cryptoDTO.setAddress("349drsd");
        cryptoDTO.setIsDeleted(Boolean.FALSE);
        ResponeEntity res = webClient.put()
                .uri("/api/cryptos/{id}",cryptoDTO.getId())
                .body(Mono.just(cryptoDTO),CryptoDTO.class)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
        String symbol = String.valueOf (((LinkedHashMap) res.getData()).get("symbol"));
        assertEquals(cryptoDTO.getSymbol(),symbol);
    }
    
    @Test
    void deleteCrypto() {
        Long id = 18698L;
        ResponeEntity res = webClient.delete()
                .uri("/api/cryptos/{id}",id)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
        String message = String.valueOf (res.getMessage());
        assertEquals("Xoá thành công",message);
    }
    
    @Test
    void exchangeCrypto() {
        String fromSymbol = "BTC",toSymBol = "ETH";
        BigDecimal amount = BigDecimal.ONE;
        ResponeEntity res = webClient.get()
                .uri("/api/cryptos/exchange?fromSymbol={fromSymbol}&fromAmount={amount}&toSymbol={toSymbol}",fromSymbol,amount,toSymBol)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
        BigDecimal amountEx = BigDecimal.valueOf((Double) (res.getData()));
        assertNotNull(amountEx);
    }
    
    @Test
    void whiteListCollateral() {
    }
    
    @Test
    void whiteListSupply() {
    }
}