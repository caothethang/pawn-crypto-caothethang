package com.ctthang.cryptoservice.mapper;

import com.ctthang.cryptoservice.dto.CryptoDTO;
import com.ctthang.cryptoservice.entity.Crypto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Class chuyển đổi giữa DTO và entity của Crypto
 */
@Component
public class CryptoMapper {
    
    /**
     * Chuyển từ Crypto -> CryptoDTO
     * @param crypto
     * @return
     */
    public CryptoDTO toDTO(Crypto crypto) {
        CryptoDTO cryptoDTO = new CryptoDTO();
        cryptoDTO.setId(crypto.getId());
        cryptoDTO.setAddress(crypto.getAddress());
        cryptoDTO.setSymbol(crypto.getSymbol());
        cryptoDTO.setName(crypto.getName());
        cryptoDTO.setCreatedAt(crypto.getCreatedAt());
        cryptoDTO.setUpdateAt(crypto.getUpdatedAt());
        cryptoDTO.setIsDeleted(crypto.getIsDeleted());
        cryptoDTO.setWhitelistCollateral(crypto.getWhitelistCollateral());
        crypto.setWhitelistSupply(crypto.getWhitelistSupply());
        cryptoDTO.setCoinGeckoId(crypto.getCoinGeckoId());
        cryptoDTO.setUrl(crypto.getUrl());
        return cryptoDTO;
    }
    
    /**
     * Chuyển từ CryptoDTO -> Crypto
     * @param cryptoDTO
     * @return
     */
    public Crypto toCrypto(CryptoDTO cryptoDTO) {
        Crypto crypto = new Crypto();
        crypto.setName(cryptoDTO.getName());
        crypto.setAddress(cryptoDTO.getAddress());
        crypto.setCoinGeckoId(cryptoDTO.getCoinGeckoId());
        crypto.setSymbol(cryptoDTO.getSymbol());
        crypto.setCreatedAt(cryptoDTO.getCreatedAt());
        crypto.setUpdatedAt(cryptoDTO.getUpdateAt());
        crypto.setWhitelistCollateral(cryptoDTO.getWhitelistCollateral());
        crypto.setWhitelistSupply(cryptoDTO.getWhitelistSupply());
        crypto.setIsDeleted(cryptoDTO.getIsDeleted());
        crypto.setUrl(cryptoDTO.getUrl());
        return crypto;
    }
    
    /**
     * Đổi từ list Crypto -> list CryptoDTO
     * @param cryptos : danh sách các crypto
     * @return
     */
    public List<CryptoDTO> toListDTO(List<Crypto> cryptos) {
        List<CryptoDTO> cryptoDTOList = new ArrayList<>();
        cryptos.forEach(crypto -> cryptoDTOList.add(toDTO(crypto)));
        return cryptoDTOList;
    }
}
