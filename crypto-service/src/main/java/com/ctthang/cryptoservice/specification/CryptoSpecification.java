package com.ctthang.cryptoservice.specification;

import com.ctthang.cryptoservice.entity.Crypto;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * Custom query sử dụng Specification
 */

public class CryptoSpecification {
    
    /**
     * Tìm kiếm theo symbol,address và name
     * @param symbol
     * @param address
     * @param name
     * @return
     */
    public static Specification<Crypto> filterForCrypto(String symbol, String address, String name){
        return (root, query, criteriaBuilder) -> {
            Collection<Predicate> predicates = new ArrayList<>();
            if(! Objects.isNull(symbol)){
                predicates.add(criteriaBuilder.like(root.get("symbol"),"%"+symbol+"%"));
            }
            if (!Objects.isNull(address)){
                predicates.add(criteriaBuilder.like(root.get("address"),"%"+address+"%"));
            }
            if(!Objects.isNull(name)){
                predicates.add(criteriaBuilder.like(root.get("name"),"%"+name+"%"));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
