package com.ctthang.cryptoservice.schedule;

import com.ctthang.cryptoservice.entity.Crypto;
import com.ctthang.cryptoservice.dto.CryptoExchangeDTO;
import com.ctthang.cryptoservice.entity.CryptoRateHistory;
import com.ctthang.cryptoservice.repository.CryptoRateHistoryRepository;
import com.ctthang.cryptoservice.repository.CryptoRepository;
import com.ctthang.cryptoservice.service.CryptoService;
import com.ctthang.cryptoservice.utils.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CryptoPriceUpdate {
    
    private final CryptoRepository cryptoRepository;
    private final CryptoService cryptoServices;
    private final List<String> COINGECKO_ID_LIST;
    private final String COINGECKO_LIST_PARAM;
    private List<Crypto> cryptos;
    private final CryptoRateHistoryRepository cryptoRateHistoryRepository;
    
    public CryptoPriceUpdate(CryptoRepository cryptoRepository,
                     CryptoService cryptoServices, CryptoRateHistoryRepository cryptoRateHistoryRepository){
        this.cryptoRepository = cryptoRepository;
        this.cryptoServices = cryptoServices;
        this.COINGECKO_ID_LIST = cryptoServices.getAllCoinGeckoId();
        this.COINGECKO_LIST_PARAM = StringUtils.convertListToString(COINGECKO_ID_LIST);
        this.cryptoRateHistoryRepository = cryptoRateHistoryRepository;
    }
    
    /**
     *  Lập lịch chạy mỗi 1 phút
     *  Xoá những bản ghi quá hạn 60p
     *  cập nhật tỷ giá của các crypto
     */
    @Scheduled(cron = "0 * * * * *")
    public void getPriceForAllCryptoPerMinute(){
        // Xoá các bản ghi cũ quá 60p
        cryptoRateHistoryRepository.deleteRateHistoryAfterTime(60);
        // Lấy tỷ giá hiện tại của tất cả các crypto
        List<CryptoExchangeDTO> cryptoCurrentPriceList = cryptoServices.getCurrentPriceOfListSymbol(COINGECKO_LIST_PARAM);
        // Lấy danh sách các crypto
        cryptos = cryptoRepository.findAll();
        /**
         * Thực hiện so sánh bên trong vòng lặp
         * Nếu symbol của crypto trùng với symbol bên trong cryptoExchangeDTO sẽ thựchieenj lưu trữ tỷ giá vào bảng crypto_rate_history
         * Thực hiện remove cryptoExchangeDTO đó khỏi lần lặp tiếp theo nhằm giảm thời gian lặp
         */
        for(Crypto c : cryptos){
            for(CryptoExchangeDTO cryptoExchangeDTO : cryptoCurrentPriceList){
                if(c.getSymbol().equalsIgnoreCase(cryptoExchangeDTO.getSymbol())){
                    CryptoRateHistory cryptoRateHistory = new CryptoRateHistory(cryptoExchangeDTO.getCurrent_price(),c);
                    cryptoRateHistoryRepository.saveAndFlush(cryptoRateHistory);
                    cryptoCurrentPriceList.remove(cryptoExchangeDTO);
                    break;
                }
            }
        }
    }
}
