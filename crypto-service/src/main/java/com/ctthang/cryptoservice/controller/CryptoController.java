package com.ctthang.cryptoservice.controller;

import com.ctthang.cryptoservice.dto.CryptoDTO;
import com.ctthang.cryptoservice.dto.CryptoExchangeDTO;
import com.ctthang.cryptoservice.dto.ResponeEntity;
import com.ctthang.cryptoservice.service.CryptoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(CryptoController.URL)
public class CryptoController {
    
    public static final String URL = "/api/cryptos";
    
    @Autowired
    private CryptoService cryptoServices;
   
    
    /**
     * api lấy danh sách crypto theo phân trang
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "lấy danh sách crypto theo phân trang")
    public ResponeEntity getListCrypto(@RequestParam(value = "pageIndex", defaultValue = "0", required = false) int pageIndex,
                                       @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize) {
        List<CryptoDTO> cryptoDTOList = cryptoServices.getAll(pageIndex,pageSize);
        return new ResponeEntity(200,"Thành công",cryptoDTOList);
    }
    
    /**
     * api lấy thông tin một crypto theo id
     *
     * @param id
     * @return
     */
    @GetMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "lấy ra thông tin của một crypto")
    public ResponeEntity getCryptoById(@PathVariable long id) {
        CryptoDTO cryptoDTO = cryptoServices.getCryptoById(id);
        return new ResponeEntity(200,"Thành công",cryptoDTO);
    }
    
    /**
     * api tìm kiếm theo symbol,address,name
     *
     * @param symbol : symbol của crypto
     * @param pageIndex : chỉ mục trang
     * @param pageSize : số phần tử trên trang
     * @return
     */
    @GetMapping({"/search"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "danh sách crypto theo filter")
    public ResponeEntity getCryptoFilter(@RequestParam(value = "symbol", required = false) String symbol,
                                           @RequestParam(value = "address", required = false) String address,
                                           @RequestParam(value = "name", required = false) String name,
                                           @RequestParam(defaultValue = "0") int pageIndex,
                                           @RequestParam(defaultValue = "5") int pageSize) {
        List<CryptoDTO> cryptoDTOList = cryptoServices.findCryptoBySymbolAndNameAndAddress(symbol, name, address , pageIndex, pageSize);
        return new ResponeEntity(200,"Thành công",cryptoDTOList);
    }
    
    /**
     * Api lấy tỷ giá theo usd của 1 crypto
     * @param symbol
     * @return
     */
    @GetMapping({"/current-price"})
    @ApiOperation(value = "lấy tỷ giá theo USD của 1 crypto")
    @ResponseStatus(HttpStatus.OK)
    public ResponeEntity getCurrentPriceOfSymbol(@RequestParam(value = "symbol") String symbol) {
        CryptoExchangeDTO exchangeDTOS = cryptoServices.getCurrentPriceOfSymbol(symbol);
        return new ResponeEntity(200,"Thành công",exchangeDTOS);
    }
    
    /**
     * Api lấy tỷ giá của 1 crypto tại 1 thời điểm
     * @param symbol
     * @param time
     * @return
     */
    @GetMapping({"/price-time"})
    @ApiOperation(value = "lấy giá của crypto tại thời điểm")
    @ResponseStatus(HttpStatus.OK)
    public ResponeEntity getPriceOfSymbolAtTime(@RequestParam (value = "symbol")String symbol,
                                                @RequestParam(value = "time_at")Long time){
        BigDecimal price = cryptoServices.getPriceOfSymbolAtTime(symbol, time);
        return new ResponeEntity(200,"Thành công",price);
    }
    
    /**
     * api tạo mới một crypto
     *
     * @param cryptoDTO
     * @return
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Tạo mới 1 crypto")
    public ResponeEntity createNewCrypto(@RequestBody @Valid CryptoDTO cryptoDTO) {
        CryptoDTO crypto = cryptoServices.createNewCrypto(cryptoDTO);
        return new ResponeEntity(200,"Thành công",crypto);
    }
    
    /**
     * api cập nhật thông tin một crypto
     *
     * @param id
     * @param cryptoDTO
     * @return
     */
    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "cập nhật crypto")
    public ResponeEntity updateCrypto(@PathVariable long id, @RequestBody @Valid CryptoDTO cryptoDTO) {
        CryptoDTO cr = cryptoServices.updateCryptoById(id, cryptoDTO);
        return new ResponeEntity(200,"Thành công",cr);
    }
    
    /**
     * api xoá một crypto theo id
     *
     * @param id
     */
    @DeleteMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "xoá một crypto theo id")
    public ResponeEntity deleteCrypto(@PathVariable long id) {
        cryptoServices.deleteCryptoById(id);
        return new ResponeEntity(200,"Xoá thành công",null);
    }
    
    /**
     * api chuyển đổi giá trị amount từ crypto A -> crypto B
     * @param fromSymbol
     * @param fromAmount
     * @param toSymbol
     * @return
     */
    @GetMapping({"/exchange"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Chuyển đổi amount từ crypto A sang crypto B")
    public ResponeEntity exchangeCrypto(@RequestParam(value = "fromSymbol") String fromSymbol,
                                     @RequestParam(value = "fromAmount") BigDecimal fromAmount,
                                     @RequestParam(value = "toSymbol") String toSymbol){
        BigDecimal priceAfterExchange = cryptoServices.exchangeSymbolTo(fromSymbol,fromAmount,toSymbol);
        return new ResponeEntity(200,"Tỷ giá sau chuyển đổi",priceAfterExchange);
    }
    
    @GetMapping({"/whitelist-collateral"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Lấy danh sách collateral")
    public ResponeEntity whiteListCollateral(){
        List<CryptoDTO> whiteListCollateral = cryptoServices.currencyCollateralAvailable();
        return new ResponeEntity(200,"Thành công",whiteListCollateral);
    }
    
    @GetMapping({"/whitelist-supply"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Lấy danh sách loan currency")
    public ResponeEntity whiteListSupply(@RequestParam(value = "collateralId") Long collateralId){
        List<CryptoDTO> whiteListSupply = cryptoServices.loanCurrencyAvailable(collateralId);
        return new ResponeEntity(200,"Loan currency available",whiteListSupply);
    }
    
}
