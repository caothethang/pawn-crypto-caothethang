package com.ctthang.cryptoservice.exception_handle;

import lombok.Data;

/**
 * Exception được ném ra mỗi khi có lỗi
 */
@Data
public class CustomException extends RuntimeException{
    
    private static final long serialVersionUID = 1L;
    
    public CustomException(String s) {
        super(s);
    }
}
