package com.ctthang.cryptoservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CryptoDTO {
    
    private Long id;
    
    @NotBlank(message = "symbol is mandatory")
    private String symbol;
    
    @NotBlank(message = "address is mandatory")
    private String address;
    
    @NotBlank(message = "name is mandatory")
    private String name;
    
    @NotBlank(message = "coin_gecko_id is mandatory")
    private String coinGeckoId;
    
    @NotNull
    private Boolean isDeleted;
    
    @NotNull
    private Boolean whitelistCollateral;
    
    @NotNull
    private Boolean whitelistSupply;
    
    private String url;
    
    private Date createdAt;
    
    private Date updateAt;
}
