package com.ctthang.cryptoservice.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CryptoExchangeDTO {
    private String symbol;
    private String name;
    private BigDecimal current_price;
}
