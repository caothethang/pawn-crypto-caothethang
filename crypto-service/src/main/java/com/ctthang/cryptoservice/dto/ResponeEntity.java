package com.ctthang.cryptoservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponeEntity {
    private int status;
    private String message;
    private Object data;
}
