package com.ctthang.cryptoservice.service;

import com.ctthang.cryptoservice.dto.CryptoDTO;
import com.ctthang.cryptoservice.entity.Crypto;
import com.ctthang.cryptoservice.dto.CryptoExchangeDTO;
import com.ctthang.cryptoservice.entity.CryptoRateHistory;
import com.ctthang.cryptoservice.exception_handle.CustomException;
import com.ctthang.cryptoservice.mapper.CryptoMapper;
import com.ctthang.cryptoservice.repository.CryptoRateHistoryRepository;
import com.ctthang.cryptoservice.repository.CryptoRepository;
import com.ctthang.cryptoservice.specification.CryptoSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CryptoServiceImpl implements CryptoService{
    
    @Autowired
    private CryptoRepository cryptoRepository;
    
    @Autowired
    private CryptoMapper cryptoMapper;
    
    private final WebClient webClient;
    
    @Autowired
    private CryptoRateHistoryRepository cryptoRateHistoryRepository;
    
    //URL và URI dùng lấy dữ liệu từ coingecko.com
    private final String COIN_GECKO_URL = "https://api.coingecko.com";
    private final String COIN_GECKO_BY_IDS = "/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false&ids={listID}";
    
    public CryptoServiceImpl() {
        this.webClient = WebClient.create(COIN_GECKO_URL);
    }
    
    /**
     * Lấy danh sách crypto với phân trang , sắp xếp theo symbol tăng dần
     *
     * @param pageIndex : chỉ mục trang
     * @param pageSize : số lượng phần tử trên page
     * @return : danh sách crypto trên page
     */
    @Override
    public List<CryptoDTO> getAll(int pageIndex, int pageSize) {
        Pageable paging = PageRequest.of(pageIndex, pageSize, Sort.by("symbol").ascending());
        Page<Crypto> cryptoPage = cryptoRepository.findAll(paging);
        List<Crypto> cryptoList = cryptoPage.getContent();
        return cryptoMapper.toListDTO(cryptoList);
    }
    
    /**
     * Lấy ra thông tin một crypto
     *
     * @param id : id của crypto
     * @return cryptoDTO
     */
    @Override
    public CryptoDTO getCryptoById(Long id) throws CustomException {
        Optional<Crypto> opt = cryptoRepository.findById(id);
        if(!opt.isPresent()){
            throw new CustomException("Crypto not found");
        }
        return cryptoMapper.toDTO(opt.get());
    }
    
    /**
     * Tạo mới một crypto
     *
     * @param cryptoDTO : dữ liệu từ client
     * @return cryptoDTO : thông tin của crypto mới tạo
     */
    @Override
    public CryptoDTO createNewCrypto(CryptoDTO cryptoDTO) {
        return saveAndReturnDTO(cryptoMapper.toCrypto(cryptoDTO));
    }
    
    /**
     * Cập nhật thông tin một crypto theo id
     *
     * @param id : id của crypto
     * @param cryptoDTO : dữ liệu mới cập nhật
     * @return thông tin của crypto vừa cập nhật
     */
    @Override
    public CryptoDTO updateCryptoById(Long id, CryptoDTO cryptoDTO) {
        Crypto crypto = cryptoMapper.toCrypto(cryptoDTO);
        crypto.setId(id);
        return saveAndReturnDTO(crypto);
    }
    
    /**
     * Hàm thực hiện lưu và trả ra thông tin crypto
     *
     * @param crypto : crypto mới
     * @return thông tin của crypto vừa thực hiện lưu
     */
    private CryptoDTO saveAndReturnDTO(Crypto crypto){
        Crypto savedCrypto = cryptoRepository.save(crypto);
        return cryptoMapper.toDTO(savedCrypto);
    }
    
    /**
     * Xoá một crypto khỏi cơ sở dữ liệu
     *
     * @param id : id của crypto
     */
    @Override
    public void deleteCryptoById(Long id) {
        try {
            cryptoRepository.deleteById(id);
        } catch (Exception e) {
            throw new CustomException("Crypto not found with this ID");
        }
    }
    
    /**
     * Tìm kiếm danh sách crypto với các param name,address,symbol
     * @param symbol : symbol của crypto cần tìm
     * @param name : name của crypto cần tìm
     * @param address : address của crypto
     * @param pageIndex : chỉ mục trang
     * @param pageSize : số phần tử trên trang
     * @return : danh sách các crypto thoả mãn điều kiện tìm kiếm
     */
    @Override
    public List<CryptoDTO> findCryptoBySymbolAndNameAndAddress(String symbol, String name, String address, int pageIndex, int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("symbol").ascending());
        Page<Crypto> page = cryptoRepository.findAll(CryptoSpecification.filterForCrypto(symbol, address, name), pageable);
        List<Crypto> cryptos = page.getContent();
        return cryptoMapper.toListDTO(cryptos);
    }
    
    /**
     * lấy tỷ giá của một symbol
     *
     * @param symbol : tên symbol của crypto
     * @return : danh sách tỷ giá của  symbol
     */
    @Override
    public CryptoExchangeDTO getCurrentPriceOfSymbol(String symbol) {
        // lấy ra crypto có symbol cần tìm
        Crypto crypto = cryptoRepository.findBySymbol(symbol);
        if(Objects.isNull(crypto)){
            throw new CustomException("Không tìm thấy symbol");
        }
        CryptoExchangeDTO cryptoExchangeDTO = new CryptoExchangeDTO();
        cryptoExchangeDTO.setSymbol(symbol);
        cryptoExchangeDTO.setName(crypto.getName());
        // lấy tỷ giá mới của crypto
        CryptoRateHistory cryptoRateHistory = cryptoRateHistoryRepository.findFirstByCryptoIdOrderByCreatedAtDesc(crypto.getId());
        cryptoExchangeDTO.setCurrent_price(cryptoRateHistory.getCurrentPrice());
        return cryptoExchangeDTO;
    }
    
    /**
     * Chuyển đổi giá trị amount từ 1 symbol qua symbol khác
     * @param fromSymbol : symbol cần chuyển đổi
     * @param fromAmount : số lượng
     * @param toSymbol : symbol đích
     * @return : tỷ giá đã quy đổi
     */
    @Override
    public BigDecimal exchangeSymbolTo(String fromSymbol, BigDecimal fromAmount, String toSymbol) {
        Crypto fromCrypto = cryptoRepository.findBySymbol(fromSymbol);
        Crypto toCrypto= cryptoRepository.findBySymbol(toSymbol);
        if(Objects.isNull(fromCrypto) || Objects.isNull(toCrypto)){
            throw new CustomException("Invalid input crypto");
        }
        BigDecimal fromSymbolPrice,toSymbolPrice;
        // tỷ giá mới của 2 crypto
        fromSymbolPrice = cryptoRateHistoryRepository.findFirstByCryptoIdOrderByCreatedAtDesc(fromCrypto.getId()).getCurrentPrice();
        toSymbolPrice = cryptoRateHistoryRepository.findFirstByCryptoIdOrderByCreatedAtDesc(toCrypto.getId()).getCurrentPrice();
        BigDecimal toAmount = null;
        if (!Objects.isNull(fromSymbolPrice) && !Objects.isNull(toSymbolPrice)) {
            toAmount = fromAmount.multiply(fromSymbolPrice.divide(toSymbolPrice,5, RoundingMode.CEILING));
        }
        return toAmount;
    }
    
    /**
     * Danh sách coin gecko id trong csdl
     * @return : danh sách các coin_gecko_id của tất cả crypto
     */
    @Override
    public List<String> getAllCoinGeckoId() {
        List<Crypto> cryptos = cryptoRepository.findAll();
        List<String> listCoinGecko = cryptos.stream().map(Crypto::getCoinGeckoId).collect(Collectors.toList());
        return listCoinGecko;
    }
    
    /**
     * Lấy giá hiện tại của các crypto
     * @param ids : chuỗi gồm coin_gecko_id
     * @return : danh sách tỷ giá của các list symbol
     */
    @Override
    public List<CryptoExchangeDTO> getCurrentPriceOfListSymbol(String ids){
        List<CryptoExchangeDTO> respone = webClient.get()
                .uri(COIN_GECKO_BY_IDS, ids)
                .retrieve()
                .bodyToFlux(CryptoExchangeDTO.class)
                .collectList()
                .block();
        return respone;
    }
    
    /**
     * Giá hiện tại của Crypto
     * @param symbol : tên symbol của Crypto
     * @param timeAt : thời gian tìm kiếm
     * @return : tỷ giá của crypto tại thời điểm tìm kiếm gần nhất
     */
    @Override
    public BigDecimal getPriceOfSymbolAtTime(String symbol, Long timeAt) {
        Long crypto_id = cryptoRepository.findBySymbol(symbol).getId();
        BigDecimal priceAtTime = cryptoRateHistoryRepository.getPriceAtTimeNearest(crypto_id,timeAt);
        return priceAtTime;
    }
    
    /**
     * lấy danh sách crypto currency collateral
     * @return
     */
    @Override
    public List<CryptoDTO> currencyCollateralAvailable() {
        List<Crypto> cryptoList = cryptoRepository.findByWhitelistCollateralTrue();
        return cryptoMapper.toListDTO(cryptoList);
    }
    
    /**
     * lấy danh sách loan currency crypto
     * @param id : id của crypto đã được chọn collateral
     * @return
     */
    @Override
    public List<CryptoDTO> loanCurrencyAvailable(Long id) {
        List<Crypto> cryptoList = cryptoRepository.findByWhitelistSupplyTrueAndIdNot(id);
        return cryptoMapper.toListDTO(cryptoList);
    }
}
