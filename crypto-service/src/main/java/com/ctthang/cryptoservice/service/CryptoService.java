package com.ctthang.cryptoservice.service;

import com.ctthang.cryptoservice.dto.CryptoDTO;
import com.ctthang.cryptoservice.dto.CryptoExchangeDTO;
import com.ctthang.cryptoservice.exception_handle.CustomException;

import java.math.BigDecimal;
import java.util.List;

public interface CryptoService {
    /**
     * Lấy danh sách tất cả các crypto
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<CryptoDTO> getAll(int pageIndex, int pageSize);
    
    /**
     * Lấy thông tin một crypto theo Id
     * @param id
     * @return
     * @throws CustomException
     */
    CryptoDTO getCryptoById(Long id) throws CustomException;
    
    /**
     * Tạo mới một crypto
     * @param cryptoDTO
     * @return
     */
    CryptoDTO createNewCrypto(CryptoDTO cryptoDTO);
    
    /**
     * Cập nhật một crypto theo Id
     * @param id
     * @param cryptoDTO
     * @return
     */
    CryptoDTO updateCryptoById(Long id, CryptoDTO cryptoDTO);
    
    /**
     * Xoá một crypto theo Id
     * @param id
     */
    void deleteCryptoById(Long id);
    
    /**
     * Lọc danh sách crypto thếymbol , name và address
     * @param symbol
     * @param name
     * @param address
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<CryptoDTO> findCryptoBySymbolAndNameAndAddress(String symbol,String name,String address,int pageIndex,int pageSize);
    
    /**
     * Lấy tỷ giá hiện tại của 1 crypto
     * @param symbol : symbol của creypto
     * @return
     */
    CryptoExchangeDTO getCurrentPriceOfSymbol(String symbol);
    
    /**
     * Chuyển đổi tỷ giá amount giữa 2 crypto
     * @param fromSymbol
     * @param fromAmount
     * @param toSymbol
     * @return
     */
    BigDecimal exchangeSymbolTo(String fromSymbol, BigDecimal fromAmount, String toSymbol);
    
    /**
     * Lấy danh sách tất cả các coin_Gecko_Id
     * @return
     */
    List<String> getAllCoinGeckoId();
    
    /**
     * Lấy tỷ giá của tất cả các crypto
     * @param ids : chuỗi id các coin_gecko_id , ngăn cách bởi dấu ","
     * @return
     */
    List<CryptoExchangeDTO> getCurrentPriceOfListSymbol(String ids);
    
    /**
     * Lấy tỷ giá của 1 crypto tại một thời điểm
     * @param symbol : symbol của crypto
     * @param timeAt : thời gian (timestamp)
     * @return
     */
    BigDecimal getPriceOfSymbolAtTime(String symbol, Long timeAt);
    
    /**
     * Danh sách các crypto được hỗ trợ khi tạo Collateral
     * @return
     */
    List<CryptoDTO> currencyCollateralAvailable();
    
    /**
     * Danh sách các crypto được hỗ trợ khi tạo loan_currency
     * @param id : id của crypto đã được chọn collateral
     * @return
     */
    List<CryptoDTO> loanCurrencyAvailable(Long id);
}
