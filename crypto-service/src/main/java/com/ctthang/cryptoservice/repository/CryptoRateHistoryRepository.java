package com.ctthang.cryptoservice.repository;


import com.ctthang.cryptoservice.entity.CryptoRateHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Repository
public interface CryptoRateHistoryRepository extends JpaRepository<CryptoRateHistory,Long> {
    
    /**
     * Xoá lịch sử giá đã lưu sau 1 khoảng thời gian
     * @param timeExpired : thời gian lưu trữ tối đa (phút)
     */
    @Modifying
    @Query(value = "delete from crypto_rate_history c where TIMESTAMPDIFF(MINUTE ,c.created_at,TIMESTAMP(NOW())) > ?1",nativeQuery = true)
    @Transactional(rollbackFor = Exception.class)
    void deleteRateHistoryAfterTime(int timeExpired);
    
    /**
     * Lấy tỷ giá tại thời gian gần nhất so với thời điểm nhập vào
     * @param cryptoId : id của crypto cần lấy tỷ giá
     * @param timeAt : thời gian cần lấy tỷ giá (timestamp)
     * @return
     */
    @Query(value = "select current_price from crypto_rate_history " +
            "where crypto_id = ?1 AND UNIX_TIMESTAMP(created_at) <= ?2 " +
            "ORDER BY created_at desc limit 1",nativeQuery = true)
    BigDecimal getPriceAtTimeNearest(Long cryptoId, Long timeAt);
    
    /**
     * lấy tỷ giá mới nhất của crypto
     * @param cryptoId
     * @return
     */
    CryptoRateHistory findFirstByCryptoIdOrderByCreatedAtDesc(Long cryptoId);
}
