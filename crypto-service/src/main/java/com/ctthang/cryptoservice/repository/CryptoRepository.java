package com.ctthang.cryptoservice.repository;

import com.ctthang.cryptoservice.entity.Crypto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CryptoRepository extends JpaRepository<Crypto, Long>, JpaSpecificationExecutor<Crypto> {
    
    /**
     * Tìm kiếm crypto dựa theo symbol
     * @param symbol
     * @return
     */
    Crypto findBySymbol(String symbol);
    
    /**
     * tìm kiếm crypto có white_list_collateral = true
     * @return
     */
    List<Crypto> findByWhitelistCollateralTrue();
    
    /**
     * Tìm kiếm các crypto có white_list_supply = true và khác với collateral_id đã chọn
     * @param id
     * @return
     */
    List<Crypto> findByWhitelistSupplyTrueAndIdNot(Long id);
}
