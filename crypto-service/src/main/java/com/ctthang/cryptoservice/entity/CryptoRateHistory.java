package com.ctthang.cryptoservice.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@Table(name = "crypto_rate_history")
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@ApiModel
public class CryptoRateHistory extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "current_price")
    private BigDecimal currentPrice;
    
    @ManyToOne
    @JoinColumn(name = "crypto_id")
    private Crypto crypto;
    
    public CryptoRateHistory(BigDecimal currentPrice, Crypto c) {
        this.setCreatedAt(new Date());
        this.currentPrice = currentPrice;
        this.crypto = c;
    }
}
