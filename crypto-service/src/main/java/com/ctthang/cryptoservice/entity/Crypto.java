package com.ctthang.cryptoservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "crypto_asset")
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@ApiModel
public class Crypto extends BaseEntity{
    
    @Column(name = "symbol", nullable = false)
    private String symbol;
    
    @Column(name = "address")
    private String address;
    
    @Column(name = "is_deleted")
    private Boolean isDeleted;
    
    @Column(name = "whitelist_collateral", nullable = false)
    private Boolean whitelistCollateral;
    
    @Column(name = "whitelist_supply", nullable = false)
    private Boolean whitelistSupply;
    
    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "coin_gecko_id", nullable = false)
    private String coinGeckoId;
    
    @Column(name = "url",nullable = false)
    private String url;
    
    @OneToMany(mappedBy = "crypto",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JsonIgnore
    private List<CryptoRateHistory> cryptoRateHistories;
}

