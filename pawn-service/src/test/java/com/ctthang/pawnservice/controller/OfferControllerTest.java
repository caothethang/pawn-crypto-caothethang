package com.ctthang.pawnservice.controller;

import com.ctthang.pawnservice.dto.OfferDTO;
import com.ctthang.pawnservice.dto.ResponeEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
class OfferControllerTest {
    
    private WebClient webClient;
    private String url;
    
    @BeforeEach
    void setUp() {
        url = "http://localhost:" + 8083;
        webClient = WebClient.create(url);
    }
    
    @Test
    void sendOfferToCollateral() {
        OfferDTO offerDTO = new OfferDTO();
        offerDTO.setCollateralId(8L);
        offerDTO.setWalletAddress("offer wallet");
        offerDTO.setRepaymentCurrencyId(6L);
        offerDTO.setLoanAmount(new BigDecimal(5.5));
        offerDTO.setMessage("string offer");
        offerDTO.setLiquidThreshold(5.5);
        offerDTO.setInterestRate(new BigDecimal(50));
        offerDTO.setDurationType(0);
        offerDTO.setDurationTime(10);
        ResponeEntity res = webClient.post()
                .uri("/api/offer/create")
                .body(Mono.just(offerDTO), OfferDTO.class)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
    }
    
    @Test
    void getOfferById() {
        Long id = 2L;
        ResponeEntity res = webClient.get()
                .uri("/api/offer/{id}",id)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
    }
    
}