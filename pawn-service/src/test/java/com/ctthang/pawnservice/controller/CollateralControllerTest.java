package com.ctthang.pawnservice.controller;

import com.ctthang.pawnservice.common.CollateralStatus;
import com.ctthang.pawnservice.dto.CollateralDTO;
import com.ctthang.pawnservice.dto.ResponeEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
class CollateralControllerTest {
    
    private WebClient webClient;
    private String url;
    
    
    @BeforeEach
    void setUp() {
        url = "http://localhost:" + 8083;
        webClient = WebClient.create(url);
    }
    
    @Test
    void getListCollateral() {
        ResponeEntity res = webClient.get()
                .uri("/api/collaterals")
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        List<CollateralDTO> collaterals = (List<CollateralDTO>) res.getData();
        assertEquals(200, res.getStatus());
        assertEquals(5,collaterals.size());
    }
    
    @Test
    void getCollateralById() {
        Long id = 8L;
        ResponeEntity res = webClient.get()
                .uri("/api/collaterals/{id}",id)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
        Long collateralId = Long.valueOf(((LinkedHashMap) res.getData()).get("id").toString());
        assertEquals(200, res.getStatus());
        assertEquals(id,collateralId);
    }
    
    @Test
    void createNewCollateral() {
        CollateralDTO collateralDTO = new CollateralDTO();
        collateralDTO.setAmount(new BigDecimal(20));
        collateralDTO.setCurrencyId(2L);
        collateralDTO.setWalletAddress("new wallet address");
        collateralDTO.setMessage("test message");
        collateralDTO.setDurationType(0);
        collateralDTO.setDurationTime(10);
        collateralDTO.setLoanCurrencyId(5L);
        ResponeEntity res = webClient.post()
                .uri("/api/collaterals")
                .body(Mono.just(collateralDTO),CollateralDTO.class)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
    }
    
    @Test
    void updateCollateral() {
        CollateralDTO collateralDTO = new CollateralDTO();
        collateralDTO.setAmount(new BigDecimal(50));
        collateralDTO.setMessage("change message");
        collateralDTO.setId(24L);
        collateralDTO.setDurationType(0);
        collateralDTO.setDurationTime(10);
        collateralDTO.setLoanCurrencyId(5L);
        collateralDTO.setCurrencyId(2L);
        collateralDTO.setStatus(3);
        collateralDTO.setWalletAddress("new wallet address");
        ResponeEntity res = webClient.put()
                .uri("/api/collaterals/{id}",collateralDTO.getId())
                .body(Mono.just(collateralDTO),CollateralDTO.class)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
    }
    
    @Test
    void withDrawCollateral() {
        Long idCollateral = 24L;
        ResponeEntity res = webClient.patch()
                .uri("/api/collaterals/with-draw/{id}",idCollateral)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
        Integer status = (Integer) ((LinkedHashMap) res.getData()).get("status");
        assertEquals(CollateralStatus.WITHDRAW.value, status.intValue());
    }
    
    @Test
    void filterCollateral() {
        Long loanCurrencyCryptoId = 2L;
        Long collateralCryptoId = 3L;
        ResponeEntity res = webClient.patch()
                .uri("/api/collaterals/filter?collateral={collateralCryptoId}&loan_currency={loanCurrencyCryptoId}",collateralCryptoId,loanCurrencyCryptoId)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
    }
    
    @Test
    void acceptOfferForCollateral() {
        Long collateralId = 8L;
        Long offerId = 2L;
        ResponeEntity res = webClient.patch()
                .uri("/api/collaterals/{collateralId}/accept/{offerId}",collateralId,offerId)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        assertNotNull(res);
        Integer status = (Integer) ((LinkedHashMap) res.getData()).get("status");
        assertEquals(CollateralStatus.ACCEPTED.value, status.intValue());
    }
}