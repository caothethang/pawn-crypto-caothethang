package com.ctthang.pawnservice.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "crypto_asset")
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@ApiModel
public class Crypto{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date created_at;
    
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updated_at;
    
    @Column(name = "symbol", nullable = false)
    private String symbol;
    
    @Column(name = "address")
    private String address;
    
    @Column(name = "is_deleted")
    private Boolean is_deleted;
    
    @Column(name = "whitelist_collateral", nullable = false)
    private Boolean whitelistCollateral;
    
    @Column(name = "whitelist_supply", nullable = false)
    private Boolean whitelistSupply;
    
    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "coin_gecko_id", nullable = false)
    private String coin_gecko_id;
    
    @Column(name = "url",nullable = false)
    private String url;
    
    @OneToMany(mappedBy = "cryptoCurrency",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Collateral> currencyCollateral;
    
    @OneToMany(mappedBy = "cryptoLoanCurrency",cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Collateral> cryptoLoanCurrency;
    
}
