package com.ctthang.pawnservice.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public class BaseEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "status")
    private Integer status;
    
    @Column(name = "wallet_address")
    private String walletAddress;
    
    @Column(name = "message")
    private String message;
    
    
    @Column(name = "duration_type")
    private Integer durationType;
    
    @Column(name = "duration_time")
    private Integer durationTime;
    
    @Column(name = "created_date",updatable = false)
    @CreationTimestamp
    private Date createdDate;
    
    @Column(name = "updated_date")
    @UpdateTimestamp
    private Date updatedDate;
}
