package com.ctthang.pawnservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Getter
@Setter
@ApiModel
@Table(name = "collateral",indexes = {
        @Index(name = "wallet_address_index",columnList = "wallet_address"),
        @Index(name = "crypto_currency_id_index", columnList = "crypto_currency_id"),
})
public class Collateral extends BaseEntity implements Serializable {
    
    @Column(name = "amount")
    private BigDecimal amount;
    
    // map với id bên bảng crypto
    @ManyToOne
    @JoinColumn(name = "crypto_currency_id",referencedColumnName = "id")
    @JsonIgnore
    private Crypto cryptoCurrency;
    
    // map với id bên bảng crypto
    @ManyToOne
    @JoinColumn(name = "crypto_loan_currency_id",referencedColumnName = "id")
    @JsonIgnore
    private Crypto cryptoLoanCurrency;
    
    @OneToMany(mappedBy = "collateral",cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Offer> offers;
}
