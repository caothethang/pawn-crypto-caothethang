package com.ctthang.pawnservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "offer")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Offer extends BaseEntity{
    
    @Column(name = "loan_to_value")
    private BigDecimal loanToValue;
    
    @Column(name = "loan_amount")
    private BigDecimal loanAmount;
    
    @Column(name = "interest_rate")
    private BigDecimal interestRate;
    
    @Column(name = "ltv_liquid_threshold")
    private Double liquidThreshold;
    
    @ManyToOne
    @JoinColumn(name = "collateral_id",referencedColumnName = "id")
    @JsonIgnore
    private Collateral collateral;
    
    @ManyToOne
    @JoinColumn(name = "repayment_crypto_id",referencedColumnName = "id")
    @JsonIgnore
    private Crypto repaymentCrypto;
}
