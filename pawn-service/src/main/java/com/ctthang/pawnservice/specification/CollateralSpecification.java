package com.ctthang.pawnservice.specification;

import com.ctthang.pawnservice.entity.Collateral;
import com.ctthang.pawnservice.entity.Crypto;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class CollateralSpecification {
    
    /**
     * Thực hiện lọc collateral theo các trường
     * @param walletAdress : địa chỉ ví
     * @param loanCurrencyId : loanCurrency
     * @param collateralId : collateralId
     * @param status : trạng thái
     * @return
     */
    public static Specification<Collateral> filterForCollateral(String walletAdress, Long loanCurrencyId, Long collateralId, Integer status){
        return (root, query, criteriaBuilder) -> {
            Collection<Predicate> predicates = new ArrayList<>();
            if(! Objects.isNull(walletAdress)){
                predicates.add(criteriaBuilder.equal(root.get("walletAddress"),walletAdress));
            }
            if (!Objects.isNull(loanCurrencyId)){
                Path<Crypto> cryptoPath = root.get("cryptoLoanCurrency");
                predicates.add(criteriaBuilder.equal(cryptoPath.<Long>get("id"),loanCurrencyId));
            }
            if(!Objects.isNull(collateralId)){
                Path<Crypto> cryptoPath = root.get("cryptoCurrency");
                predicates.add(criteriaBuilder.equal(cryptoPath.get("id"),collateralId));
            }
            if(!Objects.isNull(status)){
                predicates.add(criteriaBuilder.equal(root.get("status"),status));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
    
    
}
