package com.ctthang.pawnservice.mapper;

import com.ctthang.pawnservice.dto.CryptoDTO;
import com.ctthang.pawnservice.entity.Crypto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CryptoMapper {
    
    public CryptoDTO toDTO(Crypto crypto) {
        CryptoDTO cryptoDTO = new CryptoDTO();
        cryptoDTO.setId(crypto.getId());
        cryptoDTO.setSymbol(crypto.getSymbol());
        cryptoDTO.setWhitelistCollateral(crypto.getWhitelistCollateral());
        cryptoDTO.setWhitelistSupply(crypto.getWhitelistSupply());
        cryptoDTO.setUrl(crypto.getUrl());
        return cryptoDTO;
    }
    
    public Crypto toCrypto(CryptoDTO cryptoDTO) {
        Crypto crypto = new Crypto();
        crypto.setSymbol(cryptoDTO.getSymbol());
        crypto.setWhitelistCollateral(cryptoDTO.getWhitelistCollateral());
        crypto.setWhitelistSupply(cryptoDTO.getWhitelistSupply());
        crypto.setUrl(cryptoDTO.getUrl());
        return crypto;
    }
    
    public List<CryptoDTO> toListDTO(List<Crypto> cryptos) {
        List<CryptoDTO> cryptoDTOList = new ArrayList<>();
        cryptos.forEach(crypto -> cryptoDTOList.add(toDTO(crypto)));
        return cryptoDTOList;
    }
}
