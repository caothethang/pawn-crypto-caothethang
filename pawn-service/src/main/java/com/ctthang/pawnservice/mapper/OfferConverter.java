package com.ctthang.pawnservice.mapper;

import com.ctthang.pawnservice.dto.OfferDTO;
import com.ctthang.pawnservice.entity.Offer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OfferConverter {
    
    public OfferDTO toDTO(Offer offer){
        OfferDTO offerDTO = new OfferDTO();
        offerDTO.setId(offer.getId());
        offerDTO.setMessage(offer.getMessage());
        offerDTO.setStatus(offer.getStatus());
        offerDTO.setLoanAmount(offer.getLoanAmount());
        offerDTO.setDurationTime(offer.getDurationTime());
        offerDTO.setDurationType(offer.getDurationType());
        offerDTO.setLoanToValue(offer.getLoanToValue());
        offerDTO.setLiquidThreshold(offer.getLiquidThreshold());
        offerDTO.setInterestRate(offer.getInterestRate());
        offerDTO.setRepaymentCurrencyId(offer.getRepaymentCrypto().getId());
        offerDTO.setCollateralId(offer.getCollateral().getId());
        offerDTO.setWalletAddress(offer.getWalletAddress());
        offerDTO.setCreatedDate(offer.getCreatedDate());
        return offerDTO;
    }
    
    public Offer toOfferEntity(OfferDTO offerDTO){
        Offer offer = new Offer();
        offer.setStatus(offerDTO.getStatus());
        offer.setLiquidThreshold(offerDTO.getLiquidThreshold());
        offer.setMessage(offerDTO.getMessage());
        offer.setInterestRate(offerDTO.getInterestRate());
        offer.setLoanAmount(offerDTO.getLoanAmount());
        offer.setLoanToValue(offerDTO.getLoanToValue());
        offer.setDurationType(offerDTO.getDurationType());
        offer.setDurationTime(offerDTO.getDurationTime());
        offer.setWalletAddress(offerDTO.getWalletAddress());
        return offer;
    }
    
    public List<OfferDTO> toListDTO(List<Offer> offers) {
        List<OfferDTO> offerDTOList = new ArrayList<>();
        offers.forEach(offer -> offerDTOList.add(toDTO(offer)));
        return offerDTOList;
    }
}
