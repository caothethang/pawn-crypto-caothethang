package com.ctthang.pawnservice.mapper;

import com.ctthang.pawnservice.dto.CollateralDTO;
import com.ctthang.pawnservice.entity.Collateral;
import com.ctthang.pawnservice.service.CollateralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class CollateralConverter {
    
    @Autowired
    private OfferConverter offerConverter;
    
    @Autowired
    private CollateralService collateralService;
    
    public CollateralDTO toCollateralDTO(Collateral collateral){
        CollateralDTO collateralDTO = new CollateralDTO();
        collateralDTO.setId(collateral.getId());
        collateralDTO.setStatus(collateral.getStatus());
        collateralDTO.setMessage(collateral.getMessage());
        collateralDTO.setAmount(collateral.getAmount());
        collateralDTO.setDurationTime(collateral.getDurationTime());
        collateralDTO.setDurationType(collateral.getDurationType());
        if(!Objects.isNull(collateral.getOffers())){
            collateralDTO.setOffers(offerConverter.toListDTO(collateral.getOffers()));
        }
        collateralDTO.setWalletAddress(collateral.getWalletAddress());
        collateralDTO.setCurrencyId(collateral.getCryptoCurrency().getId());
        collateralDTO.setLoanCurrencyId(collateral.getCryptoLoanCurrency().getId());
        collateralDTO.setEstimate(collateralService.evaluteEstimate(collateral));
        return collateralDTO;
    }
    
    public Collateral toCollateral(CollateralDTO collateralDTO){
        Collateral collateral = new Collateral();
        collateral.setStatus(collateralDTO.getStatus());
        collateral.setMessage(collateralDTO.getMessage());
        collateral.setAmount(collateralDTO.getAmount());
        collateral.setDurationTime(collateralDTO.getDurationTime());
        collateral.setDurationType(collateralDTO.getDurationType());
        collateral.setWalletAddress(collateralDTO.getWalletAddress());
        return collateral;
    }
    
    public List<CollateralDTO> toListDTO(List<Collateral> collaterals) {
        List<CollateralDTO> collateralDTOList = new ArrayList<>();
        collaterals.forEach(collateral -> collateralDTOList.add(toCollateralDTO(collateral)));
        return collateralDTOList;
    }
    
    
    
}
