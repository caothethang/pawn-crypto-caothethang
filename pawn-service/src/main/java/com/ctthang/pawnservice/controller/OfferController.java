package com.ctthang.pawnservice.controller;

import com.ctthang.pawnservice.dto.OfferDTO;
import com.ctthang.pawnservice.dto.ResponeEntity;
import com.ctthang.pawnservice.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = OfferController.OFFER_URL)
public class OfferController {
    
    @Autowired
    private OfferService offerService;
    
    public static final String OFFER_URL = "/api/offer/";
    
    @PostMapping({"/create"})
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional(rollbackFor = Exception.class)
    public ResponeEntity sendOfferToCollateral(@RequestBody @Valid OfferDTO offerDTO){
        OfferDTO offerCreated = offerService.sendOfferToCollateral(offerDTO);
        return new ResponeEntity(201,"Gửi offer thành công",offerCreated);
    }
    
    @GetMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public ResponeEntity getOfferById(@PathVariable long id){
        OfferDTO offerDTO = offerService.getOfferById(id);
        return new ResponeEntity(200,"Thành công",offerDTO);
    }
    
    
}
