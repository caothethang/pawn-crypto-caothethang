package com.ctthang.pawnservice.controller;

import com.ctthang.pawnservice.dto.CollateralDTO;
import com.ctthang.pawnservice.dto.ResponeEntity;
import com.ctthang.pawnservice.service.CollateralService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(CollateralController.URL)
@Api
public class CollateralController {
    
    public static final String URL = "/api/collaterals";
    
    @Autowired
    private CollateralService collateralService;
    
    /**
     * Lấy ra danh sách Collateral theo phân trang
     * @param pageIndex : chỉ mục trang
     * @param pageSize : số collateral / trang
     * @return
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponeEntity getListCollateral(@RequestParam(value = "pageIndex",defaultValue = "0",required = false)int pageIndex,
                                           @RequestParam(value = "pageSize",defaultValue = "5",required = false)int pageSize){
        List<CollateralDTO> allCollateralDTO = collateralService.getAll(pageIndex,pageSize);
        return new ResponeEntity(200,"Thành công",allCollateralDTO);
    }
    
    /**
     * Lấy thông tin một collateral theo ID
     * @param id
     * @return
     */
    @GetMapping({"/{id}"})
    public ResponeEntity getCollateralById(@PathVariable long id){
        CollateralDTO collateralDTO = collateralService.getCollateralById(id);
        return new ResponeEntity(200,"Thành công",collateralDTO);
    }
    
    /**
     * Tạo mới 1 collateral
     * @param collateralDTO : dữ liệu nhận từ Client
     * @return
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional(rollbackFor = Exception.class)
    public ResponeEntity createNewCollateral(@RequestBody @Valid CollateralDTO collateralDTO){
        CollateralDTO collateral = collateralService.createNewCollateral(collateralDTO);
        return new ResponeEntity(201,"Tạo mới thành công",collateral);
    }
    
    /**
     * Cập nhật thông tin Collateral
     * @param id
     * @param collateralDTO
     * @return
     */
    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    @Transactional(rollbackFor = Exception.class)
    public ResponeEntity updateCollateral(@PathVariable long id,@RequestBody @Valid CollateralDTO collateralDTO){
        CollateralDTO collateralUpdated = collateralService.updateCollateralById(id,collateralDTO);
        return new ResponeEntity(200,"Cập nhật thành công",collateralUpdated);
    }
    /**
     * Withdraw Collateral
     */
    @PatchMapping ({"/with-draw/{id}"})
    @ResponseStatus(HttpStatus.OK)
    @Transactional(rollbackFor = Exception.class)
    public ResponeEntity withDrawCollateral(@PathVariable long id){
        CollateralDTO collateralWithDraw = collateralService.withDrawCollateral(id);
        return new ResponeEntity(200,"Thành công",collateralWithDraw);
    }
    
    /**
     * Filter tìm kiếm Collateral
     * @param walletAddress : ví
     * @param LoanCurrencyCryptoId : id của loanCurrency crypto
     * @param collateralCryptoId : id của collateralCurrency
     * @param status : trạng thái Collateral
     * @param pageIndex : chỉ mục
     * @param pageSize : số ptu / trang
     * @return
     */
    @GetMapping({"/filter"})
    @ResponseStatus(HttpStatus.OK)
    public ResponeEntity filterCollateral(@RequestParam(value = "wallet_address",required = false) String walletAddress,
                                                @RequestParam(value = "loan_currency",required = false) Long LoanCurrencyCryptoId,
                                                @RequestParam(value = "collateral",required = false) Long collateralCryptoId,
                                                @RequestParam(value = "status",required = false) Integer status,
                                                @RequestParam(value = "pageIndex",defaultValue = "0",required = false)int pageIndex,
                                                @RequestParam(value = "pageSize",defaultValue = "5",required = false)int pageSize){
        List<CollateralDTO> filteredCollateralDTOS = collateralService.filterCollateral(walletAddress,LoanCurrencyCryptoId,collateralCryptoId,status,pageIndex,pageSize);
        return new ResponeEntity(200,"Tìm kiếm theo filter",filteredCollateralDTOS);
    }
    
    /**
     * Accept một offer của Collateral
     * @param offerId : id của offer
     * @param collateralId : id của collateral
     * @return
     */
    @PatchMapping({"/{collateralId}/accept/{offerId}"})
    @ResponseStatus(HttpStatus.ACCEPTED)
    @Transactional(rollbackFor = Exception.class)
    public ResponeEntity acceptOfferForCollateral(@PathVariable Long offerId,@PathVariable Long collateralId){
        CollateralDTO collateralDTO = collateralService.acceptOfferForCollateral(collateralId,offerId);
        return new ResponeEntity(200,"Accept",collateralDTO);
    }
}
