package com.ctthang.pawnservice.validation;

import com.ctthang.pawnservice.common.Duration;
import com.ctthang.pawnservice.dto.CollateralDTO;
import com.ctthang.pawnservice.exception_handle.ListValidationException;
import com.ctthang.pawnservice.repository.CryptoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;
import java.util.List;
import java.util.Objects;

/**
 * Class thực hiện validate collateral
 */
@Component
public class CollateralValidation {
    
    @Autowired
    private  CryptoRepository cryptoRepository;
    
    @Autowired
    private ListValidationException listException;
    
    private final String PATTERN = "[0-9]+.[0-9]{0,5}" ;
    
    public void isValid(CollateralDTO collateralDTO){
        List<ValidationException> listError = listException.getValidationExceptions();
        listError.clear();
        // kiểm tra message
        if(!Objects.isNull(collateralDTO.getMessage()) && collateralDTO.getMessage().trim().isEmpty()){
            listError.add(new ValidationException("Invalid message"));
        }
        // kiểm tra amount
        if(!Objects.isNull(collateralDTO.getAmount()) && !collateralDTO.getAmount().toString().matches(PATTERN)){
           listError.add(new ValidationException("Invalid amount"));
        }
        // kiểm tra currency id
        if(!Objects.isNull(collateralDTO.getCurrencyId()) && cryptoRepository.findByIdAndWhitelistCollateralIsTrue(collateralDTO.getCurrencyId())==null){
            listError.add(new ValidationException("Invalid collateral"));
        }
        // kiểm tra loan currency id
        if(!Objects.isNull(collateralDTO.getLoanCurrencyId()) && cryptoRepository.findByIdAndWhitelistSupplyIsTrue(collateralDTO.getLoanCurrencyId()) == null){
            listError.add(new ValidationException("Invalid loan collateral"));
        }
        // kiểm tra duration type và duration time
        if(!collateralDTO.getDurationType().equals(Duration.WEEK.value) && !collateralDTO.getDurationType().equals(Duration.MONTH.value)){
            listError.add(new ValidationException("Invalid DurationType"));
        }
        // nếu là week
        if(collateralDTO.getDurationType().equals(Duration.WEEK.value) && collateralDTO.getDurationTime() > 5200){
            listError.add(new ValidationException("Durtion by week can not greater than 5200"));
        }
        // nếu là month
        if(collateralDTO.getDurationType().equals(Duration.MONTH.value) && collateralDTO.getDurationTime() > 1200){
            listError.add(new ValidationException("Durtion by month can not greater than 1200"));
        }
        if(listError.size()>0){
            throw listException;
        }
    }
    
}
