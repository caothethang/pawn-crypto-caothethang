package com.ctthang.pawnservice.validation;

import com.ctthang.pawnservice.common.Duration;
import com.ctthang.pawnservice.dto.OfferDTO;
import com.ctthang.pawnservice.entity.Collateral;
import com.ctthang.pawnservice.exception_handle.CustomException;
import com.ctthang.pawnservice.exception_handle.ListValidationException;
import com.ctthang.pawnservice.repository.CollateralRepository;
import com.ctthang.pawnservice.repository.CryptoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class OfferValidation {
    
    @Autowired
    private CollateralRepository collateralRepository;
    
    @Autowired
    private ListValidationException listValidationException;
    
    @Autowired
    private CryptoRepository cryptoRepository;
    
    public void validateOffer(OfferDTO offerDTO){
        List<ValidationException> listError = listValidationException.getValidationExceptions();
        listError.clear();
        Collateral collateral;
        Optional<Collateral> optionalCollateral;
        // kiểm tra collateral null
        if(Objects.isNull(offerDTO.getCollateralId())){
            throw new CustomException("Invalid collateral id");
        }else {
        // kiểm tra liệu collateral tồn tại trong db không
            optionalCollateral = collateralRepository.findById(offerDTO.getCollateralId());
            if(!optionalCollateral.isPresent()){
                throw new CustomException("Collateral isn't found");
            }
        }
        collateral = optionalCollateral.get();
        // kiểm tra message null hoặc empty
        if(Objects.isNull(offerDTO.getMessage()) || offerDTO.getMessage().isEmpty()){
            listError.add(new ValidationException("Invalid Message"));
        }
        // kiểm tra loanToValue null hoặc âm
        if(Objects.isNull(offerDTO.getLoanToValue()) || offerDTO.getLoanToValue().doubleValue()<0){
            listError.add(new ValidationException("Invalid Loan to value"));
        }
        // kiểm tra loanAmount null hoặc bị âm
        if(Objects.isNull(offerDTO.getLoanAmount()) || offerDTO.getLoanAmount().doubleValue() < 0){
            listError.add(new ValidationException("Invalid Loan amount"));
        }
    
        // kiểm tra interest rate null hoặc bị ấm
        if(Objects.isNull(offerDTO.getInterestRate()) || offerDTO.getInterestRate().doubleValue() < 0){
            listError.add(new ValidationException("Invalid interest rate"));
        }
    
        //kiểm tra liquidation threshold null hoặc bị âm
        if(Objects.isNull(offerDTO.getLiquidThreshold()) || offerDTO.getLiquidThreshold().doubleValue() < 0){
            listError.add(new ValidationException("Invalid Liquidation Threshold"));
        }
    
        // kiểm tra duration vs recurringInterest = weekly
        if(Objects.isNull(offerDTO.getDurationType()) ||Objects.isNull(offerDTO.getDurationTime())  || offerDTO.getDurationType().equals(Duration.WEEK.value) && offerDTO.getDurationTime() > 5200){
            listError.add(new ValidationException("Durtion by week can not greater than 5200"));
        }
    
        //kiểm tra duration vs recurringInterest = monthly , duration null
        if(Objects.isNull(offerDTO.getDurationType()) ||Objects.isNull(offerDTO.getDurationTime())  || offerDTO.getDurationType().equals(Duration.MONTH.value) && offerDTO.getDurationTime() > 1200){
            listError.add(new ValidationException("Durtion by month can not greater than 1200"));
        }
        Long idOfDFY = cryptoRepository.findCryptoBySymbol("DFY").getId();
        // kiểm tra repayment token có null và id có thuộc DFY hoặc Loan token
        if(Objects.isNull(offerDTO.getRepaymentCurrencyId()) || !offerDTO.getRepaymentCurrencyId().equals(idOfDFY) && !offerDTO.getRepaymentCurrencyId().equals(collateral.getCryptoLoanCurrency().getId())){
            listError.add(new ValidationException("Invalid Repayment Currency"));
        }
        if(listError.size()>0){
            throw listValidationException;
        }
    
    
    }
}
