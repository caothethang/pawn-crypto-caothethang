package com.ctthang.pawnservice.service;

import com.ctthang.pawnservice.dto.CollateralDTO;
import com.ctthang.pawnservice.entity.Collateral;

import java.math.BigDecimal;
import java.util.List;

public interface CollateralService {
    
    /**
     * Lấy danh sách Collateral
     * @param pageIndex : chỉ mục trang
     * @param pageSize : số lượng phần tử / trang
     * @return
     */
    List<CollateralDTO> getAll(int pageIndex, int pageSize);
    
    /**
     * Tạo mới collateral
     * @param collateralDTO
     * @return
     */
    CollateralDTO createNewCollateral(CollateralDTO collateralDTO);
    
    /**
     * Cập nhật thông tin một collateral
     * @param id
     * @param collateralDTO
     * @return
     */
    CollateralDTO updateCollateralById(long id, CollateralDTO collateralDTO);
    
    /**
     * Lấy thông tin của collateral
     * @param id : id của collateral
     * @return : CollateralDTO
     */
    CollateralDTO getCollateralById(long id);
    
    /**
     * Thực hiện withdraw một collateral
     * @param id : id của collateral
     * @return
     */
    CollateralDTO withDrawCollateral(long id);
    
    /**
     * Tìm kiếm collateral dựa theo fileeeeeer
     * @param walletAddress : địa chỉ wallet
     * @param loanCurrencyCryptoId : loanCurrency
     * @param collateralCryptoId : collateralCrypto
     * @param status : trạng thái
     * @param pageIndex : chỉ mục trang
     * @param pageSize : số ptu / trang
     * @return
     */
    List<CollateralDTO> filterCollateral(String walletAddress, Long loanCurrencyCryptoId, Long collateralCryptoId, Integer status,int pageIndex,int pageSize);
    
    /**
     *  lấy ra entity collateral
     * @param id
     * @return : Collateral
     */
    Collateral getCollateralEntity(long id);
    
    /**
     * Chấp nhận 1 offer
     * @param collateralId : id của collateral
     * @param offerId : id của offer
     * @return : Collateral đã được accept
     */
    CollateralDTO acceptOfferForCollateral(Long collateralId, Long offerId);
    
    /**
     * Tính toán estimate cho collateral
     * @param collateral : collateral
     * @return : BigDecimal : tỷ giá
     */
    BigDecimal evaluteEstimate(Collateral collateral);
}
