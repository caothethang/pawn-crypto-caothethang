package com.ctthang.pawnservice.service.impl;

import com.ctthang.pawnservice.common.CollateralStatus;
import com.ctthang.pawnservice.common.OfferStatus;
import com.ctthang.pawnservice.dto.CollateralDTO;
import com.ctthang.pawnservice.dto.ResponeEntity;
import com.ctthang.pawnservice.entity.Collateral;
import com.ctthang.pawnservice.entity.Offer;
import com.ctthang.pawnservice.exception_handle.CustomException;
import com.ctthang.pawnservice.mapper.CollateralConverter;
import com.ctthang.pawnservice.repository.CollateralRepository;
import com.ctthang.pawnservice.repository.OfferRepository;
import com.ctthang.pawnservice.service.CollateralService;
import com.ctthang.pawnservice.service.CryptoService;
import com.ctthang.pawnservice.specification.CollateralSpecification;
import com.ctthang.pawnservice.validation.CollateralValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

@Service
public class CollateralServiceImpl implements CollateralService {
    
    @Autowired
    private OfferRepository offerRepository;
    
    @Autowired
    private CollateralRepository collateralRepository;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CollateralValidation collateralValidation;
    
    @Autowired
    private CollateralConverter collateralConverter;
    
    /**
     * Lấy ra danh sách các collateral
     * @param pageIndex : chỉ mục trang
     * @param pageSize : số lượng phần tử / trang
     * @return
     */
    @Override
    public List<CollateralDTO> getAll(int pageIndex, int pageSize) {
        Pageable paging = PageRequest.of(pageIndex, pageSize);
        Page<Collateral> page = collateralRepository.findAll(paging);
        List<Collateral> collaterals = page.getContent();
        return collateralConverter.toListDTO(collaterals);
    }
    
    /**
     * Tạo mới collateral
     * @param collateralDTO
     * @return
     */
    @Override
    public CollateralDTO createNewCollateral(CollateralDTO collateralDTO) {
        // kiểm tra input collateralDTO
        collateralValidation.isValid(collateralDTO);
        // set status là OPEN
        collateralDTO.setStatus(CollateralStatus.OPEN.value);
        // thực hiện convert sang entity
        Collateral collateral = collateralConverter.toCollateral(collateralDTO);
        // Tìm kiếm các crypto theo Id và thực hiện set thuộc tính theo khoá ngoại
        collateral.setCryptoCurrency(cryptoService.findCryptoWithId(collateralDTO.getCurrencyId()));
        collateral.setCryptoLoanCurrency(cryptoService.findCryptoWithId(collateralDTO.getLoanCurrencyId()));
        // lưu collateral
        Collateral collateralSaved = collateralRepository.saveAndFlush(collateral);
        return collateralConverter.toCollateralDTO(collateralSaved);
    }
    
    @Override
    public CollateralDTO updateCollateralById(long id, CollateralDTO collateralDTO) {
        collateralValidation.isValid(collateralDTO);
        Collateral collateral = collateralConverter.toCollateral(collateralDTO);
        collateral.setCryptoCurrency(cryptoService.findCryptoWithId(collateralDTO.getCurrencyId()));
        collateral.setCryptoLoanCurrency(cryptoService.findCryptoWithId(collateralDTO.getLoanCurrencyId()));
        collateral.setId(id);
        Collateral collateralSaved = collateralRepository.saveAndFlush(collateral);
        return collateralConverter.toCollateralDTO(collateralSaved);
    }
    
    @Override
    public CollateralDTO getCollateralById(long id) {
        // kiểm tra tồn tại của Collateral
        Optional<Collateral> opt = collateralRepository.findById(id);
        if(!opt.isPresent()){
            throw new CustomException("Collateral not found");
        }
        
        Collateral collateral = opt.get();
        // thực hiện tính toán estimate
        BigDecimal estimate = evaluteEstimate(collateral);
        CollateralDTO collateralDTO = collateralConverter.toCollateralDTO(collateral);
        collateralDTO.setEstimate(estimate);
        return collateralDTO;
    }
    
    /**
     *
     * @param id : id của collateral
     * @return
     */
    @Override
    public CollateralDTO withDrawCollateral(long id) {
        // kiểm tra tồn tại collateral
        Optional<Collateral> opt = collateralRepository.findById(id);
        if(!opt.isPresent()){
            throw new CustomException("Collateral not found");
        }
        Collateral collateral = opt.get();
        // kiểm tra trạng thái nếu OPEN mới có thể withdraw
        if(collateral.getStatus() != CollateralStatus.OPEN.value){
            throw new CustomException("Collateral is't OPEN");
        }
        // đổi trạng thái của Collateral về WITHDRAW
        collateral.setStatus(CollateralStatus.WITHDRAW.value);
        // Chuyển trạng thái các Offer liên quan về CANCELED
        List<Offer> offers = collateral.getOffers();
        offers.forEach(offer -> {
            offer.setStatus(OfferStatus.CANCELED.value);
            offerRepository.saveAndFlush(offer);
        });
        collateralRepository.saveAndFlush(collateral);
        return collateralConverter.toCollateralDTO(collateral);
    }
    
    /**
     * Lọc theo specification đã tạo
     * @param walletAddress : địa chỉ wallet
     * @param loanCurrencyCryptoId : loanCurrency
     * @param collateralCryptoId : collateralCrypto
     * @param status : trạng thái
     * @param pageIndex : chỉ mục trang
     * @param pageSize : số ptu / trang
     * @return
     */
    @Override
    public List<CollateralDTO> filterCollateral(String walletAddress, Long loanCurrencyCryptoId, Long collateralCryptoId, Integer status,int pageIndex,int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex, pageSize);
        Page<Collateral> page = collateralRepository.findAll(CollateralSpecification.filterForCollateral(walletAddress,loanCurrencyCryptoId,collateralCryptoId,status), pageable);
        List<Collateral> collaterals = page.getContent();
        return collateralConverter.toListDTO(collaterals);
    }
    
    /**
     * Lấy về entity Collateral
     * @param id : id của Collateral
     * @return
     */
    @Override
    public Collateral getCollateralEntity(long id) {
        Optional<Collateral> optional = collateralRepository.findById(id);
        if(!optional.isPresent()){
            throw new CustomException("Collateral resource not found");
        }
        Collateral collateral = optional.get();
        return collateral;
    }
    
    /**
     * Accept 1 offer
     * @param collateralId : id của collateral
     * @param offerId : id của offer
     * @return
     */
    @Override
    public CollateralDTO acceptOfferForCollateral(Long collateralId, Long offerId) {
        // kiểm tra tồn tại Collateral
        Collateral collateral = collateralRepository.findById(collateralId).orElseThrow(()-> new CustomException("Collateral resource not found"));
        // kiểm tra trạng thái của Collateral
        if(collateral.getStatus() != CollateralStatus.OPEN.value){
            throw new CustomException("Collateral isn't open !");
        }
        // set trạng thái là ACCEPTED
        collateral.setStatus(CollateralStatus.ACCEPTED.value);
        // lấy danh sách tất cả các offer
        List<Offer> offersOfCollateral = collateral.getOffers();
        offersOfCollateral.forEach(offer -> {
            // chuyển các offer khác về rejected nếu Id khác với offerId được chọn
            if(offer.getId() == offerId){
                offer.setStatus(OfferStatus.ACCEPTED.value);
            }else {
                offer.setStatus(OfferStatus.REJECTED.value);
            }
            offerRepository.saveAndFlush(offer);
        });
        return collateralConverter.toCollateralDTO(collateral);
    }
    
    /**
     * Thực hiện tính toán estimate
     * @param collateral : collateral
     * @return
     */
    @Override
    public BigDecimal evaluteEstimate(Collateral collateral) {
        // lấy amount hiện tại của Collateral
        BigDecimal amount = collateral.getAmount();
        // lấy dữ liệu được gọi từ Crypto-Service
        ResponeEntity responeEntity = cryptoService.getCurrentPriceOfSymbol(collateral.getCryptoCurrency().getSymbol());
        LinkedHashMap data = (LinkedHashMap) responeEntity.getData();
        BigDecimal price = BigDecimal.valueOf((Double.parseDouble(data.get("current_price").toString())));
        BigDecimal estimate = amount.multiply(price);
        return estimate;
    }
    
}
