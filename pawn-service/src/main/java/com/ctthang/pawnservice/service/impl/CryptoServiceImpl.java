package com.ctthang.pawnservice.service.impl;

import com.ctthang.pawnservice.dto.ResponeEntity;
import com.ctthang.pawnservice.entity.Crypto;
import com.ctthang.pawnservice.exception_handle.CustomException;
import com.ctthang.pawnservice.repository.CryptoRepository;
import com.ctthang.pawnservice.service.CryptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Optional;

@Service
public class CryptoServiceImpl implements CryptoService {
    
    private final WebClient webClient;
    
    @Autowired
    private CryptoRepository cryptoRepository;
    
    private final String API_CRYPTO_SERVICE_URL ="http://localhost:9191";
    private final String API_CRYPTO_SERVICE_URI = "/api/cryptos/current-price?symbol={symbol}";
    
    public CryptoServiceImpl() {
        this.webClient = WebClient.create(API_CRYPTO_SERVICE_URL);
    }
    
    /**
     * Lấy tỷ giá của crypto
     * @param symbol : tên symbol của crypto
     * @return
     */
    @Override
    public ResponeEntity getCurrentPriceOfSymbol(String symbol) {
        ResponeEntity respone = webClient.get()
                .uri(API_CRYPTO_SERVICE_URI,symbol)
                .retrieve()
                .bodyToMono(ResponeEntity.class)
                .block();
        return respone;
    }
    
    /**
     * Lấy thông tin Crypto dựa theo Id
     * @param id
     * @return
     */
    public Crypto findCryptoWithId(Long id){
        Optional<Crypto> opt = cryptoRepository.findById(id);
        if(! opt.isPresent()){
            throw new CustomException("Crypto isn't found");
        }
        Crypto crypto = opt.get();
        return crypto;
    }
    
    
}
