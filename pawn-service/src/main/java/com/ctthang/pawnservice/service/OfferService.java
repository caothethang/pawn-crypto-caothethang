package com.ctthang.pawnservice.service;

import com.ctthang.pawnservice.dto.OfferDTO;

public interface OfferService{
    
    /**
     * Lấy thông tin offer
     * @param id : id của offer
     * @return : OfferDTO
     */
    OfferDTO getOfferById(long id);
    
    /**
     * Gửi 1 offer tới Collateral
     * @param offerDTO : thông tin của offer
     * @return : OfferDTO
     */
    OfferDTO sendOfferToCollateral(OfferDTO offerDTO);
    
}
