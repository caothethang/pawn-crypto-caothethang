package com.ctthang.pawnservice.service;

import com.ctthang.pawnservice.dto.ResponeEntity;
import com.ctthang.pawnservice.entity.Crypto;

public interface CryptoService {
    
    /**
     * lấy tỷ giá hiện tại của crypto
     * @param symbol : tên symbol của crypto
     * @return
     */
    ResponeEntity getCurrentPriceOfSymbol(String symbol);
    
    /**
     * Tìm kiếm crypto theo id
     * @param id
     * @return : Crypto
     */
    Crypto findCryptoWithId(Long id);
    
    
}
