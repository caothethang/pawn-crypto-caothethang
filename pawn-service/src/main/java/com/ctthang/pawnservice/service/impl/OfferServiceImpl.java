package com.ctthang.pawnservice.service.impl;

import com.ctthang.pawnservice.common.OfferStatus;
import com.ctthang.pawnservice.dto.OfferDTO;
import com.ctthang.pawnservice.entity.Offer;
import com.ctthang.pawnservice.exception_handle.CustomException;
import com.ctthang.pawnservice.mapper.OfferConverter;
import com.ctthang.pawnservice.repository.OfferRepository;
import com.ctthang.pawnservice.service.CollateralService;
import com.ctthang.pawnservice.service.CryptoService;
import com.ctthang.pawnservice.service.OfferService;
import com.ctthang.pawnservice.validation.OfferValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OfferServiceImpl implements OfferService {
    
    @Autowired
    private OfferConverter offerConverter;
    
    @Autowired
    private OfferRepository offerRepository;
    
    @Autowired
    private CollateralService collateralService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private OfferValidation offerValidation;
    
    /**
     * Lấy thông tin của Offer
     * @param id
     * @return
     */
    @Override
    public OfferDTO getOfferById(long id) {
        Optional<Offer> optionalOffer = offerRepository.findById(id);
        if(!optionalOffer.isPresent()){
            throw new CustomException("Resource Offer not found");
        }
        Offer offer = optionalOffer.get();
        return offerConverter.toDTO(offer);
    }
    
    /**
     * Gửi offer đến một collateral
     * @param offerDTO
     * @return
     */
    @Override
    public OfferDTO sendOfferToCollateral(OfferDTO offerDTO)
    {
        // thực hiện validate offer
        offerValidation.validateOffer(offerDTO);
        Offer offer = offerConverter.toOfferEntity(offerDTO);
        offer.setCollateral(collateralService.getCollateralEntity(offerDTO.getCollateralId()));
        offer.setRepaymentCrypto(cryptoService.findCryptoWithId(offerDTO.getRepaymentCurrencyId()));
        offer.setStatus(OfferStatus.OPEN.value);
        Offer offerOpened = offerRepository.saveAndFlush(offer);
        return offerConverter.toDTO(offerOpened);
    }
    
    
}
