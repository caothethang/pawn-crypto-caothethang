package com.ctthang.pawnservice.exception_handle;

/**
 * Exception gồm message được trả ra
 */
public class CustomException extends RuntimeException{
    
    public CustomException(String s) {
        super(s);
    }
}
