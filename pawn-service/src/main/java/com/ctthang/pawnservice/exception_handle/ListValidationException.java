package com.ctthang.pawnservice.exception_handle;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;

/**
 * Exception chứa các validation
 */
@Getter
@Setter
@Component
public class ListValidationException extends RuntimeException{
    
    private List<ValidationException> validationExceptions;
    
    public ListValidationException() {
        validationExceptions = new ArrayList<>();
    }
}
