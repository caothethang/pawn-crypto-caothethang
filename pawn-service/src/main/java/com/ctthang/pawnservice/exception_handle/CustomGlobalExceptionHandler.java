package com.ctthang.pawnservice.exception_handle;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Thực hiện bắt lỗi chương trình
 */
@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {
    
    /**
     * Thực hiện bắt mọi lỗi Exception
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    protected ResponseEntity<Object> handleAllException(
            Exception ex, WebRequest request) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    /**
     * Thực hiện bắt các lỗi CustomException
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(value = CustomException.class)
    protected ResponseEntity<Object> handleCustomException(CustomException ex,WebRequest request){
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("errors", ex.getMessage());
        return new ResponseEntity<>(body,HttpStatus.NOT_FOUND);
    }
    
    /**
     * Thực hiện bắt các lỗi thuộc về input (offer , collateral)
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(value = ListValidationException.class)
    protected ResponseEntity<Object> handleValidationException(ListValidationException ex,WebRequest request){
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        //Get all errors
        List<String> errors = ex.getValidationExceptions().stream()
                .map(x -> x.getMessage())
                .collect(Collectors.toList());
        body.put("errors", errors);
        return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
    }
    
    /**
     * Bắt các exception from annotation
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());
        
        //Get all errors
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());
        
        body.put("errors", errors);
        return new ResponseEntity<>(body, headers, status);
    }
    
}
