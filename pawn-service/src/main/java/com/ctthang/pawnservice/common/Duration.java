package com.ctthang.pawnservice.common;


public enum Duration {
    WEEK(0),MONTH(1);
    
    public Integer value;
    
    Duration(Integer value){
        this.value = value;
    }
}
