package com.ctthang.pawnservice.common;

public enum OfferStatus {
    
    OPEN(3), ACCEPTED(7), REJECTED(8), CANCELED(9);
    
    public int value;
    
    OfferStatus(int value) {
        this.value = value;
    }
}
