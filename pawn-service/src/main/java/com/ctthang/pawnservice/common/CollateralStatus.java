package com.ctthang.pawnservice.common;

public enum CollateralStatus {
    
    OPEN(3),ACCEPTED(6),WITHDRAW(7);
    
    public int value;
    
    CollateralStatus(int value) {
        this.value = value;
    }
}
