package com.ctthang.pawnservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CryptoDTO {
    
    private Long id;
    
    private String symbol;
    
    private Boolean whitelistCollateral;
    
    private Boolean whitelistSupply;
    
    private String url;
    
}
