package com.ctthang.pawnservice.dto;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Api
public class OfferDTO extends BaseDTO{
    
    @NotNull(message = "Invalid Loan To Value")
    private BigDecimal loanToValue;
    
    @NotNull(message = "Invalid Loan Amount")
    private BigDecimal loanAmount;
    
    @NotNull(message = "Invalid Interest Rate")
    private BigDecimal interestRate;
    
    @NotNull(message = "Invalid Liquid Threshold")
    private Double liquidThreshold;
    
    private Date createdDate;
    
    @NotNull(message = "Invalid Repayment Currency")
    private Long repaymentCurrencyId;
    
    @NotNull(message = "Invalid CollateralID")
    private Long collateralId;

}
