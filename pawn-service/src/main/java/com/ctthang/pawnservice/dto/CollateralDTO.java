package com.ctthang.pawnservice.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class CollateralDTO extends BaseDTO{
    
    private BigDecimal estimate;
    
    @NotNull(message = "Invalid Amount")
    private BigDecimal amount;
    
    @NotNull(message = "Invalid currency Id")
    private Long currencyId;
    
    @NotNull(message = "Invalid Loan Currency Id")
    private Long loanCurrencyId;
    
    private List<OfferDTO> offers = new ArrayList<>();
}
