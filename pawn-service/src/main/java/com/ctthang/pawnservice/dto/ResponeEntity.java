package com.ctthang.pawnservice.dto;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponeEntity {
    private int status;
    private String message;
    private Object data;
}
