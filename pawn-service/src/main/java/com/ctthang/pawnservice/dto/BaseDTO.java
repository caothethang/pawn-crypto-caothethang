package com.ctthang.pawnservice.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class BaseDTO {
    
    private Long id;
    
    private Integer status;
    
    @NotBlank(message = "Invalid wallet address")
    @NotNull(message = "Invalid wallet address")
    private String walletAddress;
    
    @NotBlank(message = "Invalid message")
    @NotNull(message = "Invalid message")
    private String message;
    
    @NotNull(message = "Duration type is required")
    private Integer durationType;
    
    @NotNull(message = "Duration time is required")
    private Integer durationTime;
    
}
