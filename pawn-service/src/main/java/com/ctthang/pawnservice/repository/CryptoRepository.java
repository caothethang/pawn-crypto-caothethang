package com.ctthang.pawnservice.repository;

import com.ctthang.pawnservice.entity.Crypto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CryptoRepository extends JpaRepository<Crypto,Long> {
    Crypto findByIdAndWhitelistCollateralIsTrue(long id);
    
    Crypto findByIdAndWhitelistSupplyIsTrue(long id);
    
    Crypto findCryptoBySymbol(String symbol);
}
